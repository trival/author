MochaWeb?.testOnly ->


  describe "a group of tests", ->


    it "should respect equality", ->
      expect 5
        .to.equal 5


    it "should stub functions", ->
      stub = sinon.stub()

      stub()

      expect stub
        .to.be.called
