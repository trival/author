MochaWeb?.testOnly ->

  describe 'Document', ->
    Document = App.entities.schemas.Document

    it 'validates objects', ->

      document =
        id: 'foo'
        rootElement: 'someId'
        styleSheet: 'someId'
        title: 'My super article'
        scripts: [ name: 'myscript', code: 'some js code', lang: 'CoffeeScript' ]


      ctx = Document.newContext()
      ctx.validate document

      expect ctx.isValid()
        .to.be.true


