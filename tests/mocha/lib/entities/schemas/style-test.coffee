MochaWeb?.testOnly ->

  describe 'Style', ->
    Style = App.entities.schemas.Style

    it 'validates objects', ->

      style =
        id: 'foo'
        styleSheet: 'styleSheetId'
        element: 'elId'
        isInstance: true
        code: 'some code'


      ctx = Style.newContext()
      ctx.validate style

      expect ctx.isValid()
        .to.be.true



