MochaWeb?.testOnly ->

  describe 'StyleSheet', ->
    StyleSheet = App.entities.schemas.StyleSheet

    it 'validates objects', ->

      styleSheet =
        id: 'foo'
        title: 'My super StyleSheet'
        styles: [ name: 'myscript', code: 'some js code' ]
        lang: 'CSS'


      ctx = StyleSheet.newContext()
      ctx.validate styleSheet

      expect ctx.isValid()
        .to.be.true



