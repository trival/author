MochaWeb?.testOnly ->

  describe 'Element', ->
    Element = App.entities.schemas.Element

    it 'validates objects', ->

      element =
        id: 'foo'
        typeId: 'bar'
        uniqueId: 'myId'
        className: 'myfoo mybar'
        attributes: [ key: 'bar', value: 'foo' ]
        children: [ 'id1', 'id2' ]


      ctx = Element.newContext()
      ctx.validate element

      expect ctx.isValid()
        .to.be.true

