MochaWeb?.testOnly ->

  describe 'ElementType', ->
    ElementType = App.entities.schemas.ElementType

    it 'validates objects', ->

      elementType =
        id: 'foo'
        name: 'header 1'
        description: 'the main header'
        tagName: 'h1'
        className: 'foo bar'
        attributes: [ {key: 'bar', value: 'fuu', isParameter: true} ]
        children: [ 'id1', 'id2' ]
        editable: true


      ctx = ElementType.newContext()
      ctx.validate elementType

      expect ctx.isValid()
        .to.be.true
