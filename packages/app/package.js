Package.describe({
    summary: 'setup the App namespace',
});

Package.onUse(function(api) {
    api.use('coffeescript');
    api.export('App');
    api.addFiles('setup.coffee');
});

