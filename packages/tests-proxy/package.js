Package.describe({
	name: "velocity:test-proxy",
	summary: "Dynamically created package to expose test files to mirrors",
	version: "0.0.4",
	debugOnly: true
});

Package.onUse(function (api) {
	api.use("coffeescript", ["client", "server"]);
	api.add_files("tests/mocha/client/sample-client-test.coffee",["client"]);
	api.add_files("tests/mocha/lib/entities/schemas/document.coffee",["server","client"]);
	api.add_files("tests/mocha/lib/entities/schemas/element-test.coffee",["server","client"]);
	api.add_files("tests/mocha/lib/entities/schemas/element-type-test.coffee",["server","client"]);
	api.add_files("tests/mocha/lib/entities/schemas/style-sheet-test.coffee",["server","client"]);
	api.add_files("tests/mocha/lib/entities/schemas/style-test.coffee",["server","client"]);
	api.add_files("tests/mocha/server/sample-server-test.coffee",["server"]);
	api.add_files("tests/mocha/setup.coffee",["server","client"]);
});