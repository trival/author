Package.describe({
    summary: 'entities of the author app, including models and schema',
});

Package.onUse(function(api) {
    api.use('coffeescript');
    api.use('app');
    api.use('aldeed:simple-schema');
    api.addFiles('setup.coffee');

    // Schemas
    api.addFiles('schemas/item.coffee');
    api.addFiles('schemas/element-type.coffee');
    api.addFiles('schemas/element.coffee');
    api.addFiles('schemas/document.coffee');
    api.addFiles('schemas/style-sheet.coffee');
    api.addFiles('schemas/style.coffee');

    // Models
    api.addFiles('models/element-type.coffee');
});

