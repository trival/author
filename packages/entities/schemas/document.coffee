schemas = App.entities.schemas

schemas.Document = new SimpleSchema [

  schemas.Item,

  rootElement:
    type: String

  styleSheet:
    type: String

  title:
    type: String

  'scripts.$.name':
    type: String
  'scripts.$.code':
    type: String
  'scripts.$.lang':
    type: String
    allowedValues: ['JavaScript', 'CoffeeScript']

]


