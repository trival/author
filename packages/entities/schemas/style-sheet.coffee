schemas = App.entities.schemas

schemas.StyleSheet = new SimpleSchema [

  schemas.Item,

  title:
    type: String

  'styles.$.name':
    type: String
  'styles.$.code':
    type: String

  lang:
    type: String
    allowedValues: ['CSS', 'Stylus']

]



