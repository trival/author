schemas = App.entities.schemas

schemas.ElementType = new SimpleSchema [

  schemas.Item,

  name:
    type: String

  description:
    type: String

  tagName:
    type: String

  className:
    type: String

  'attributes.$.key':
    type: String
  'attributes.$.value':
    type: String
  'attributes.$.isParameter':
    type: Boolean
    optional: true

  children:
    type: [String]

  editable:
    type: Boolean

]
