schemas = App.entities.schemas

schemas.Element = new SimpleSchema [

  schemas.Item,

  typeId:
    type: String

  uniqueId:
    type: String

  className:
    type: String

  'attributes.$.key':
    type: String
  'attributes.$.value':
    type: String

  children:
    type: [String]

]

